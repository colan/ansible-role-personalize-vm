Colan's VM Personalizer
=======================

Colan's script to personalize his shell account on freshly created VMs.

Requirements
------------

* `sudo` access on the target host(s).

High-Level Tasks
----------------

See [the main tasks file](tasks/main.yml).

Role Variables
--------------

See the [default variables](defaults/main.yml) file.

Dependencies
------------

None.

Example Playbook
----------------

```yml
- hosts: vm4.example.com
  roles:
    - colan.personalize-vm
  vars:
    personalize_vm_local_dir_data: "/home/jdoe/dot-files"
```

Testing
-------

Tests can be run like so (with more or fewer "v"s for verbosity):

```sh
ansible-playbook -vv --ask-become-pass --inventory vm4.example.com, /path/to/this/role/tests/test.yml
```

Feel free to add your own tests in `tests/`, using existing ones as examples.  

License
-------

AGPLv3

Author Information
------------------

[Colan Schwartz](https://colan.consulting/)